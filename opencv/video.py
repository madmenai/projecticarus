# Imports
import numpy as np
import cv2
import keyboard

# Constants
VIDEO_SOURCE = 0   # USB Video Receiver


def run_video_capture():
    """ Captures Video From a Camera """
    vcap = cv2.VideoCapture(VIDEO_SOURCE)  # Specifying a Video Source

    while True:
        if keyboard.is_pressed('esc'):
            # Stop video stream if 'Esc' is clicked
            break

        ret, frame = vcap.read()  # Reading a frame
        cv2.imshow('frame', frame) # Drawing the frame
        cv2.waitKey(1)  # Waiting for 1ms

    vcap.release()  # Releasing video capturer 
    cv2.destroyAllWindows()  # Closing active windows


if __name__ == '__main__':
    """ Main Entry Point """
    print("Starting video capture...\nPress Esc button to quit.")
    run_video_capture()
    print("Bye!")
