# Imports
import keyboard
import numpy as np
import cv2, threading
import tensorflow as tf
from keras.applications import imagenet_utils
from keras.applications import VGG16
from keras.applications import ResNet50

# Constants
CAMERA_ID = 1

# Globals
threads = []
is_frame_ready = False
is_recognition_on = False
orig_frame = None
label = ''
score = 0
model = None
graph = None


class RecognitionThread(threading.Thread):
    def __init__(self, id, name):
        global threads
        threading.Thread.__init__(self)
        self.id = id
        self.name = name
        threads.append(self)
    
    def run(self):
        global orig_frame
        global label
        global score
        global is_frame_ready
        global is_recognition_on
        global model
        global graph

        print("Starting Thread {} with ID {}...".format(self.name, self.id))

        while True:
            if is_frame_ready and orig_frame is not None:
                is_frame_ready = False
                
                with graph.as_default():
                    frame = cv2.resize(orig_frame, (224, 224))
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB).astype(np.float32)
                    frame = frame.reshape((1, ) + frame.shape)
                    frame = imagenet_utils.preprocess_input(frame)
                    predictions = model.predict(frame)
                    (imageID, label, score) = imagenet_utils.decode_predictions(predictions)[0][0]
                

def load_network():
    """ Loads a network """
    global model
    global graph
    print('Loading network...')
    model = ResNet50(weights='imagenet')
    graph = tf.get_default_graph()
    print('Network has been loaded successfully!')

def get_video():
    vcap = cv2.VideoCapture(CAMERA_ID)
    return vcap

def run_recognition():
    global orig_frame
    global label
    global score
    global is_recognition_on
    global is_frame_ready

    is_recognition_on = True

    while True:
        if keyboard.is_pressed('esc'):
            # Stop video stream and object recognition if 'Esc' is clicked
            is_recognition_on = False
            break
            
        retval, orig_frame = vcap.read()
        is_frame_ready = True
        cv2.putText(orig_frame, "Label: %s | Score: %.2f" % (label, score), (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)
        cv2.imshow("Classification", orig_frame)
        cv2.waitKey(1)

if __name__ == '__main__':
    """ Main Entry Point """
    print("Starting Real-Time Object Recognition System...")
    load_network()
    vcap = get_video()
    thread_objrec = RecognitionThread(1, 'Object Recognition Thread')
    thread_objrec.start()
    run_recognition()
    vcap.release()
    cv2.destroyAllWindows()
