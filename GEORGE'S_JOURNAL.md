﻿# Welcome to George's Drone Project Journal!

Hey! Welcome to my journal! Here you can find my daily/weekly/monthly reports about the project in a descending order (newest first), some notes, ideas, links, and other interesting stuff.

----------


## Journal

### October, 2018

#### Day 35
Still playing with the parameters. We have no time for a GUI, so we are going to build a tiny UDP Server and a Client.

#### Day 32
A test of an alpha version of the flight path planning algorithm was successful. Still, need to tune flight parameters.

#### Day 31
Trained several CNN Models on the new RGBD Dataset. Built a real-time depth prediction algorithm and post-processing routine. Starting to implement a flight path planning algorithm.

### September, 2018

#### Day 6
Converted all of the RAW data from NYU v2 RGBD Dataset to images. Starting to train some old models on this new set of 200k+ images. The neural networks are a combination of CNN layers and fully-connected layers at the end. Networks take an image as an input and output a depth map of this image.
Observation #1: 
Models are misbehaving badly. They need sticks and no carrots!  Model training takes a lot of time. 


----------


## Notes & Thoughts

 - ~~To use, or not to use fully-connected layers, that is the question!~~ CNNs work fine! ResNet helps a lot! No fully-connected layers for now.

----------


## Links
#### Project
Code Repository: https://bitbucket.org/madmenai/projecticarus/
Tasks: https://neiron.atlassian.net/projects/DA
#### Data
NYU Depth Dataset V2: https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html