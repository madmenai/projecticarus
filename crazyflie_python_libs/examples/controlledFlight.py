# Imports
import time
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
from cflib.crazyflie.syncLogger import SyncLogger
import keyboard
from threading import Thread
import droneAction

DEBUG = False

class ControlledFlight:
    """ Autonomous Flying Functions"""

    def __init__(self, link_uri):
        """ 
        Initializes a Connection to the Crazyflie
        Creates Log Callbacks
        Starts Autonomous Flying Sequence
        """
        self.CF_MAX_HEIGHT = 0.5  # Max Height when Crazyflie is in Hover Mode in m
        self.CF_STEP_SIZE = 0.04
        self.CF_TURN_SIZE = 10

        self._link_uri = link_uri
        self._cf = Crazyflie(rw_cache='./cache')  # Crazyflie object
        self.real_z = 0  # Current Z Coordinate (height)
        
        self._keyToAction = dict()

        # Connecting callbacks from the Crazyflie API
        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)

        # Trying to connect to the Crazyflie
        if not DEBUG: 
            self._cf.open_link(self._link_uri)

        self.is_connected = True  # Are we connected to the Crazyflie?

    def _connected(self, link_uri):
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""
        print('Connected to %s' % link_uri)

        # The definition of the logconfig can be made before connecting
        self._lg_params = LogConfig(name='Location', period_in_ms=10)
        self._lg_params.add_variable('stateEstimate.z', 'float')

        # Adding Z Coord Logging
        try:
            self._cf.log.add_config(self._lg_params)
            # This callback will receive the data
            self._lg_params.data_received_cb.add_callback(self._log_on_data_receive)
            # This callback will be called on errors
            self._lg_params.error_cb.add_callback(self._log_on_error)
            # Start the logging
            self._lg_params.start()
        except KeyError as e:
            print('Could not start log configuration,'
                  '{} not found in TOC'.format(str(e)))
        except AttributeError:
            print('Could not add Stabilizer log config, bad configuration.')

    def _log_on_error(self, logconf, msg):
        """Callback from the log API when an error occurs"""
        print('Error when logging %s: %s' % (logconf.name, msg))

    def _log_on_data_receive(self, timestamp, data, logconf):
        """Callback froma the log API when data arrives"""
        self.real_z = data['stateEstimate.z']

    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the speficied address)"""
        print('Connection to %s failed: %s' % (link_uri, msg))
        self.is_connected = False

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        print('Connection to %s lost: %s' % (link_uri, msg))

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""
        print('Disconnected from %s' % link_uri)
        self.is_connected = False

    def start_flight(self):
        print('Connecting to %s' % self._link_uri)

        self.flight_finished = False

        # Starting the Autonomous Flight Thread
        th_ctrl_flight = Thread(target=self.start_controlled_flight)
        th_ctrl_flight.start()

        # # Collect events until released
        # key_listener = Listener(on_press=self._on_press)
        # key_listener.start()
        keyboard.on_press(self._on_press)

        # Waiting for flight to finish
        th_ctrl_flight.join()

        # keyboard.unhook_all()

        # # Waiting for key listener thread to finish
        # key_listener.stop()
        # key_listener.join()


    def register_action(self, action, key=None):
        if not key or key not in self._keyToAction:
            action.set_drone_controller(self)
            self._keyToAction[key] = action
        else:
            raise Exception('Cannot register action: key %s already taken' % action.key)
        
    def _on_press(self, event):
        """ Keyboard Key Press Event """
        key = event.name
        if key in self._keyToAction:
            self._curr_action = self._keyToAction[key]

    def start_controlled_flight(self):
        self.curr_z = 0 # software z coordinate

        self._curr_action = None

        while not self.flight_finished:
            if not self._curr_action:
                continue
            # print(type(self._curr_action).__name__)
            # print('curr_z %f, real_z %f' % (self.curr_z, self.real_z))
            self._curr_action.execute()
            if self._curr_action.finished:
                self._curr_action = self._curr_action.next_action
                
        self._finish_flight()

    def _finish_flight(self):
        """ Routine that should be done after AutoSeq is over """
        if not DEBUG: 
            self._cf.commander.send_stop_setpoint()
            self._cf.close_link()


def init_actions(drone_controller):
    # declare actions
    takeOff = droneAction.TakeOff()
    stabilize = droneAction.Stabilize()
    land = droneAction.Land()
    stop = droneAction.Stop()
    hover = droneAction.Hover()
    moveForward = droneAction.MoveForward()
    moveBackward = droneAction.MoveBackward()
    moveLeft = droneAction.MoveLeft()
    moveRight = droneAction.MoveRight()
    turnLeft = droneAction.TurnLeft()
    turnRight = droneAction.TurnRight()

    # set next where applicable
    takeOff.next_action = stabilize

    # register actions on drone controller
    drone_controller.register_action(takeOff, 'enter')
    drone_controller.register_action(stabilize)
    drone_controller.register_action(land, 'space')
    drone_controller.register_action(stop, 'esc')
    drone_controller.register_action(hover, 'f1')
    drone_controller.register_action(moveForward)
    drone_controller.register_action(moveBackward)
    drone_controller.register_action(moveLeft)
    drone_controller.register_action(moveRight)
    drone_controller.register_action(turnLeft)
    drone_controller.register_action(turnRight)

    # print instructions
    print('--------------------------------------------------------------------------------')
    for (key,action) in drone_controller._keyToAction.items():
        print('%-10s - %s' % (type(action).__name__, key))
    print('--------------------------------------------------------------------------------')

if __name__ == '__main__':
    # URI of the Crazyflie to connect to
    uri = 'radio://0/80/2M'

    # Initializing the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)

    # Starting the Autonomous Flight Sequence
    drone_controller = ControlledFlight(uri)

    init_actions(drone_controller)

    drone_controller.start_flight()
