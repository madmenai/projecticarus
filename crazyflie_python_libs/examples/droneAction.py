import time

class DroneAction:
    def __init__(self, key=None, next_action=None):
        self._droneController = None
        self.next_action = next_action
        self.finished = False

    def set_drone_controller(self, droneController):
        self._droneController = droneController

    def execute(self):
        raise NotImplementedError

class TakeOff(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use

        if controller.real_z > controller.CF_MAX_HEIGHT:
            self.finished = True
            return
        
        controller.curr_z += controller.CF_STEP_SIZE
        controller._cf.commander.send_hover_setpoint(0, 0, 0, controller.curr_z)
        time.sleep(0.2)

class Stabilize(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use

        controller._cf.commander.send_hover_setpoint(0, 0, 0, controller.curr_z)

class Land(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use

        while controller.real_z > 0.05:
            controller.curr_z  = controller.real_z - controller.CF_STEP_SIZE
            controller._cf.commander.send_hover_setpoint(0, 0, 0, controller.curr_z)
        controller.flight_finished = True

class Stop(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use

        controller._cf.commander.send_stop_setpoint()
        controller.flight_finished = True

class Hover(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use

        controller._cf.commander.send_hover_setpoint(0, 0, 0, controller.CF_MAX_HEIGHT)

class MoveForward(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use
        
        vx = controller.CF_STEP_SIZE
        vy = 0
        yawrate = 0
        controller._cf.commander.send_hover_setpoint(vx, vy, yawrate, controller.curr_z)

class MoveBackward(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use
        
        vx = -controller.CF_STEP_SIZE
        vy = 0
        yawrate = 0
        controller._cf.commander.send_hover_setpoint(vx, vy, yawrate, controller.curr_z)

class MoveLeft(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use
        
        vx = 0
        vy = controller.CF_STEP_SIZE
        yawrate = 0
        controller._cf.commander.send_hover_setpoint(vx, vy, yawrate, controller.curr_z)

class MoveRight(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use
        
        vx = 0
        vy = -controller.CF_STEP_SIZE
        yawrate = 0
        controller._cf.commander.send_hover_setpoint(vx, vy, yawrate, controller.curr_z)

class TurnLeft(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use
        
        vx = 0
        vy = 0
        yawrate = controller.CF_TURN_SIZE
        controller._cf.commander.send_hover_setpoint(vx, vy, yawrate, controller.curr_z)

class TurnRight(DroneAction):
    def execute(self):
        controller = self._droneController # for ease of use
        
        vx = 0
        vy = 0
        yawrate = -controller.CF_TURN_SIZE
        controller._cf.commander.send_hover_setpoint(vx, vy, yawrate, controller.curr_z)
