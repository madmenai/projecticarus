# Imports
import time
import os
import sys
import keyboard
sys.path.insert(0, os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
from cflib.crazyflie.syncLogger import SyncLogger
from pynput.keyboard import Key, Listener
from threading import Thread

# URI of the Crazyflie to connect to
uri = 'radio://0/80/2M'

# Some Constants
CF_MAX_HEIGHT = 0.40  # Max Height when Crazyflie is in Hover Mode in cm

class AutonomousSeq:
    """ Autonomous Flying Functions"""

    def __init__(self, link_uri):
        """ 
        Initializes a Connection to the Crazyflie
        Creates Log Callbacks
        Starts Autonomous Flying Sequence
        """
        
        self._cf = Crazyflie(rw_cache='./cache')  # Crazyflie object
        self._coord_z = 0  # Current Z Coordinate (height)

        # Connecting callbacks from the Crazyflie API
        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)
        self.seq_done = False

        print('Connecting to %s' % link_uri)

        # Trying to connect to the Crazyflie
        self._cf.open_link(link_uri)

        self.is_connected = True  # Are we connected to the Crazyflie?

        # Starting the Autonomous Flight Thread
        th_auto_flight = Thread(target=self.start_autonomous_flight)
        th_auto_flight.start()

        # Collect events until released
        with Listener(on_press=self.on_press) as listener:
            listener.join()

    def on_press(self, key):
        """ Keyboard Key Press Event """
        if key == Key.esc:
            # Terminate the Drone
            self.seq_done = True
            self._cf.commander.send_stop_setpoint()
            self.finish()

    def _connected(self, link_uri):
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""
        print('Connected to %s' % link_uri)

        # The definition of the logconfig can be made before connecting
        self._lg_params = LogConfig(name='Location', period_in_ms=10)
        self._lg_params.add_variable('stateEstimate.z', 'float')

        # Adding Z Coord Logging
        try:
            self._cf.log.add_config(self._lg_params)
            # This callback will receive the data
            self._lg_params.data_received_cb.add_callback(self._log_on_data_receive)
            # This callback will be called on errors
            self._lg_params.error_cb.add_callback(self._log_on_error)
            # Start the logging
            self._lg_params.start()
        except KeyError as e:
            print('Could not start log configuration,'
                  '{} not found in TOC'.format(str(e)))
        except AttributeError:
            print('Could not add Stabilizer log config, bad configuration.')

    def _log_on_error(self, logconf, msg):
        """Callback from the log API when an error occurs"""
        print('Error when logging %s: %s' % (logconf.name, msg))

    def _log_on_data_receive(self, timestamp, data, logconf):
        """Callback froma the log API when data arrives"""
        self._coord_z = data['stateEstimate.z']

    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the speficied address)"""
        print('Connection to %s failed: %s' % (link_uri, msg))
        self.is_connected = False

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        print('Connection to %s lost: %s' % (link_uri, msg))

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""
        print('Disconnected from %s' % link_uri)
        self.is_connected = False

    def start_autonomous_flight(self):
        """ Autonomous Flight Sequence """
        step = 0.05  # Step of Z Coord change in cm
        current_action = 'TakeOff'  # Current Action
        curr_z = 0  # Current Software Z Coordinate in cm
        yaw = 0  # Current Yaw in deg/s

        while not self.seq_done:
            real_z = self.get_z()

            if current_action is 'TakeOff':
                curr_z += step
                self._cf.commander.send_hover_setpoint(0, 0, yaw, curr_z)
                time.sleep(0.2)

                if real_z > CF_MAX_HEIGHT:
                    current_action = 'Dance'
            elif current_action is 'Dance':
                yaw = -180

                for i in range(12):
                    yaw = - yaw
                    self._cf.commander.send_hover_setpoint(0, 0, yaw, curr_z)
                    time.sleep(i / 10)

                current_action = 'Stabilize'
            elif current_action is 'Stabilize':
                for _ in range(5):
                    self._cf.commander.send_hover_setpoint(0, 0, 0, curr_z)
                    time.sleep(1)
                
                current_action = 'Jump'
            elif current_action is 'Jump':
                for _ in range(5):
                    self._cf.commander.send_hover_setpoint(0, 0, 0, curr_z * 1.5)
                    time.sleep(1)

                current_action = 'Land'
            elif current_action is 'Land':
                self.land()
                self.seq_done = True
            else:
                self.land()
                self.seq_done = True

        if self.get_z() > 0.15:
            self.land()
        
        time.sleep(3)
        self.seq_done = True

    def land(self):
        """ Lands the Crazyflie """
        while(self.get_z() > 0.05):
            curr_z  = self.get_z() - 0.04
            self._cf.commander.send_hover_setpoint(0, 0, 0, curr_z)

        self._cf.commander.send_stop_setpoint()

    def finish(self):
        """ Routine that should be done after AutoSeq is over """
        self._cf.close_link()

    def get_z(self):
        """ Returns Crazyflie's Z Coordinate """
        return self._coord_z


if __name__ == '__main__':
    # Initializing the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)

    # Starting the Autonomous Flight Sequence
    auto_seq = AutonomousSeq(uri)

    while auto_seq.is_connected:
        if auto_seq.seq_done:
            auto_seq.finish()

        time.sleep(1)
