import logging
import time
from threading import Thread
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
import cflib
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie

logging.basicConfig(level=logging.ERROR)
URI = 'radio://0/80/2M'

if __name__ == '__main__':
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)
    # Scan for Crazyflies and use the first one found
    print('Scanning interfaces for Crazyflies...')
    available = cflib.crtp.scan_interfaces()
    print('Crazyflies found:')

    for i in available:
        print(i[0])

    with SyncCrazyflie(URI, cf=Crazyflie(rw_cache='./cache')) as scf:
        cf = scf.cf
        print('Setting init thrust to 0')
        cf.commander.send_setpoint(0,0,0,0)
        print('Sleeping for 0.5ms')
        time.sleep(0.5)
        print('Enabling AltHold Mode')
        cf.param.set_value('flightmode.althold','True')
        print('Holding altitude for 10 seconds')
        ms = 0
        cf.commander.send_setpoint(0,0,0,50000)
        cf.param.set_value('flightmode.althold','True')

        while ms < 1000:
            time.sleep(0.01)
            ms += 1

        print('Closing connection')
        cf.commander.send_setpoint(0,0,0,0)
        cf.close_link()
