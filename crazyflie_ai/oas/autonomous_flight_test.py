""" This file contains depth prediction functions """

# Imports
import cv2  # Computer Vision Library
import models  # ANN Models
import keyboard  # Keyboard Control Library
import threading  # Multithreading Library
import numpy as np  # Scientific Library
import tensorflow as tf  # Numerical Library for AI Modelling
from PIL import Image  # Image Processing Library
from matplotlib import pyplot as plt  # Plotting Library
from post_processing import process_image  # Image Post-Processing Functions

# Constants
CAMERA_ID = 1  # Device ID of the Main Camera
PATH_MODELS = r''  # Model Location
X = 0  # X coordinate tuple index
Y = 1  # Y coordinate tuple index

# Globals
thread_list = []  # List of Current Threads
is_frame_ready = False  # Indicates if a Frame was successfully captured
is_predictor_on = False  # Indicates if Prediction Script is running
cv_original_frame = None  # Original Image captured from the Camera
tf_session = None  # Current Environment for Numerical Operations
tf_input_layer = None  # Input Layer of our Neural Network
tf_network = None  # Neural Network used for making Predictions
# Height of an Image which will be passed to the Input Layer of our Neural Network
tf_input_height = 228
# Width of an Image which will be passed to the Input Layer of our Neural Network
tf_input_width = 304
# Channel Count of an Image which will be passed to
# the Input Layer of our Neural Network
tf_input_channels = 3
tf_input_batch_size = 1  # We are making a Prediction for one Image at a time
path_models = PATH_MODELS  # Path to a Folder where Models are located
cv_video_capturer = None  # Video Capture Object


class DepthAnalyzerThread(threading.Thread):
    """ Class for a Depth Analyzing Routine """

    def __init__(self, thread_id, name):
        """ Constructor """
        # Initializing a Thread Object
        global thread_list
        # Calling a Base Class Constructor
        threading.Thread.__init__(self)
        self.thread_id = thread_id  # ID of a Thread
        self.name = name  # Name of a Thread
        # Adding the Current Thread Object to the List of Thread Objects
        thread_list.append(self)

    def run(self):
        """ Runs a Depth Analyzer Script """
        global cv_original_frame
        global is_frame_ready
        global is_predictor_on

        print("Starting Thread {} with ID {}...".format(self.name, self.thread_id))
        # Preparing a Window for showing our Depth Predictions
        plt.imshow(np.ones((100, 100)))
        plt.ion()
        plt.show(block=False)

        # Starting a Depth Prediction Routine
        while True:
            # Checking if we have a new Image from the Video Capturer
            if is_frame_ready and cv_original_frame is not None:
                is_frame_ready = False  # Lowering the Flag
                # Converting an Array to an Image
                image = Image.fromarray(cv_original_frame)
                # Resizing the Image so it matches the Input Layer Dimensions
                image = image.resize([tf_input_width, tf_input_height],
                                     Image.ANTIALIAS)
                # Converting Pixel Values to a `float32` Type
                image = np.array(image).astype('float32')
                # Adding a new Axis
                image = np.expand_dims(np.asarray(image), axis=0)
                # Making our Prediction for a Current Image
                prediction = tf_session.run(tf_network.get_output(),
                                            feed_dict={tf_input_layer: image})
                # Displaying our Prediction
                image = prediction[0, :, :, 0]
                image = image * 255 / image.max()
                plt.imshow(image, interpolation='nearest')
                plt.draw()
                depth_map, image_center, map_center = process_image(image)

                if depth_map is not None:
                    # TODO: Rewrite this code
                    fig, ax = plt.subplots()
                    ax.imshow(image)
                    ax.plot(map_center[X], map_center[Y], 'r.', markersize=14)
                    ax.plot(image_center[X], image_center[Y], 'b.', markersize=14)
                    ax.arrow(image_center[X], image_center[Y], map_center[X] - image_center[X],
                             map_center[Y] - image_center[Y])
                    plt.show()

                plt.pause(0.0001)
                plt.close()

                if calculate_direction:
                    # Calculating flight direction
                    image_center[X] - map_center[X]


def tf_load_network():
    """ Loads a network """
    global tf_session
    global tf_input_layer
    global tf_network
    # Creating a TF Session
    tf_session = tf.Session()
    print('Loading the Model...')
    # Creating an Input Layer for our Neural Network
    tf_input_layer = tf.placeholder(
        tf.float32, shape=(None, tf_input_height, tf_input_width, tf_input_channels))
    # Building our Neural Network
    tf_network = models.ResNet50UpProj(
        {'data': tf_input_layer}, tf_input_batch_size, 1, False)
    # Loading Parameters of the Network from Files
    saver = tf.train.Saver()
    saver.restore(tf_session, path_models)


def cv_capture_video():
    """ Captures and shows Video """
    global cv_original_frame
    global is_predictor_on
    global is_frame_ready
    global cv_video_capturer

    is_predictor_on = True

    while True:
        if keyboard.is_pressed('esc'):
            # Stop video stream and object recognition if 'Esc' is clicked
            is_predictor_on = False
            break

        # Getting a Frame from the Camera
        retval, cv_original_frame = cv_video_capturer.read()
        # Raising the Flag
        is_frame_ready = True
        # Displaying the Frame
        cv2.imshow("Camera Input", cv_original_frame)
        cv2.waitKey(1)


def main():
    """ Main Function """
    global cv_video_capturer

    # Welcome Message
    print("Starting Real-Time Depth Analyzing System...")
    # Checking if a Path to a Model Folder is different from a Placeholder
    assert path_models is not r'', \
        "Please change the Default Path of a Models Folder!"

    # Loading a Neural Network for Depth Analyzing
    tf_load_network()
    # Creating a Video Capture Object
    cv_video_capturer = cv2.VideoCapture(CAMERA_ID)
    cv_video_capturer.set(cv2.CAP_PROP_FPS, 15)()
    # Starting a Thread which will run a Depth Analyzing Script
    thread_depth_analyzer = DepthAnalyzerThread(1, 'Depth Analyzer Thread')
    thread_depth_analyzer.start()
    # Starting a Video Capture
    cv_capture_video()
    # Stopping a Video Capturer after Everything is done
    cv_video_capturer.release()
    # Closing Windows
    cv2.destroyAllWindows()


if __name__ == '__main__':
    """ Main Entry Point """
    main()
