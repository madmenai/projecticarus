# This file contains a progress bar widget for Jupyter Notebooks

# Imports
from ipywidgets import IntProgress, HTML, VBox
from IPython.display import display, clear_output

class ProgressBar:
    """ Progress Bar Widget """
    
    def __init__(self, total_elements, increment_value, step, info_text):
        """ Constructor """
        
        self.total_elements = total_elements
        self.step = step
        self.increment_value = increment_value
        self.info_text = info_text
        self.current_element = 0
        self.progress_bar = IntProgress(min=0, max=total_elements, value=0)
        self.label = HTML()
        self.label.value = '{info_text}: {index} / {total}'.format(
                info_text=self.info_text, 
                index=self.current_element, 
                total=self.total_elements)
        self.box = VBox(children=[self.label, self.progress_bar])
    
    def show(self):
        """ Displays a progress bar in a Jupyter Notebook """
        display(self.box)
    
    def update(self):
        """ Updates the current progress bar """       
        if self.current_element % self.step == 0:
            self.progress_bar.value = self.current_element
    
    def increment(self, increment_value=None):
        """ Increments the current value of the progress bar """
        if increment_value is None:
            increment_value = self.increment_value
        
        self.current_element += increment_value
        self.label.value = '{info_text}: {index} / {total}'.format(
        info_text=self.info_text, 
        index=self.current_element, 
        total=self.total_elements)
        
    def increment_update(self, increment_value=None):
        self.increment(increment_value)
        self.update()
    