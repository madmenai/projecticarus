import pandas as pd
import preproc as data
from skimage import io
from skimage.transform import resize

RESIZED_DATASET_DIR = '/media/slowdrive/nyu_dataset/subset/resized/'
COL_IMAGE = 'Image'
COL_PATH = 'Path'

def resize_images(image_list, size):
    """ Resizes every image in a list """
    width = size[0]
    height = size[1]
    resized_images = [resize(x, (height, width)) for x in image_list]
    return resize_images


def read_images(path_list):
    """ Reads every image from a list of their paths """
    images = [io.imread(x) for x in path_list]
    df_images = pd.DataFrame(data={COL_IMAGE: images, COL_PATH: path_list})
    return df_images


def save_images(df_image, directory, affix=('', '')):
    """ Saves images """
    for _, image in df_image.iterrows():
        name = image[COL_PATH]
        name_splited = name.split('.')
        name = affix[0] + name_splited[0] + affix[1] + '.' + name_splited[1]
        io.imsave(name, image[COL_IMAGE])


if __name__ == '__main__':
    """ Main Entry Point """
    x_train, y_train, x_val, y_val = data.get_data()
    df_x_val_images = read_images(x_val)
    res_images = resize_images(df_x_val_images.iloc[:, 0], (320, 240))
    df_x_val_images.iloc[:, 0] = res_images
    save_images(df_x_val_images, RESIZED_DATASET_DIR, ('', '_resized'))
