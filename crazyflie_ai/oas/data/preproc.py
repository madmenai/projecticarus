import os
import random
import numpy as np

DATASET_DIR = '/media/slowdrive/nyu_dataset/subset/'  # 
RANDOM_SEED = 9876
DATA_SPLIT = 0.1
random.seed(RANDOM_SEED)


def get_images():
    """ Gets Pathes to Images from a Directory """
    im_paths = []
    for root, _, files in os.walk(os.path.abspath(DATASET_DIR)):
        for file in files:
            im_paths.append(os.path.join(root, file))
    return im_paths


def sort_images(im_paths, order='asc'):
    """ Sorts Images """
    # A Path to an Image has the following format:
    # /media/slowdrive/nyu_dataset/subset/ID_TYPE.jpg, 
    # where ID is an integer and TYPE = rgb, map, shows if it's
    # a real image (rgb), or its depth representation (map)
    sign = 1 if order is 'asc' else -1
    im_paths = np.array(sorted(im_paths, key=lambda x: sign*int(x.split('/')[-1].split('_')[0])))
    return im_paths


def separate_images(im_paths):
    """ Separates Images """
    im_rgb = np.array([x for x in im_paths if 'rgb' in x])
    im_depth = np.array([x for x in im_paths if 'map' in x])
    return im_rgb, im_depth


def get_sets(im_rgb, im_depth):
    """ Splits the data into Train / Validation Sets """
    indices = np.arange(0, len(im_rgb))
    random.shuffle(indices)
    im_rgb = im_rgb[indices]
    im_depth = im_depth[indices]
    val_count = int(len(im_rgb) * DATA_SPLIT)
    x_train = im_rgb[:-val_count]
    y_train = im_depth[:-val_count]
    x_val = im_rgb[-val_count:]
    y_val = im_depth[-val_count:]
    return x_train, y_train, x_val, y_val


def get_data():
    """ Reads the NYU Subset Data and splits it into the Train / Val Sets """
    im_paths = get_images()
    im_paths = sort_images(im_paths)
    im_rgb, im_depth = separate_images(im_paths)
    x_train, y_train, x_val, y_val = get_sets(im_rgb, im_depth)
    return x_train, y_train, x_val, y_val


if __name__ == '__main__':
    """ Main Entry Point """
    get_data()
