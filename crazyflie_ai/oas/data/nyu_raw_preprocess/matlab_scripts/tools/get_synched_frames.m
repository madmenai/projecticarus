% Returns a struct with synchronized RGB and depth frames, as well as the
% accelerometer data. Note that this script considers the depth frames as
% 'primary' in the sense that it keeps every depth frame and matches the
% nearest RGB frame.
%
% Args:
%   scene_dir - the directory containing the raw kinect dump for a
%   particular scene.
%
% Returns:
%   frame_list - a struct containing a list of frames.
function frame_list = get_synched_frames(scene_dir)

% Getting files from the directory
files = dir(scene_dir);
% Sorting files by their names
files = sort(string({files.name}));

% Getting each type of scene and count them.
frame_list = []; % List of frames
depth_images = files(startsWith(files, 'd')); % Depth files
rgb_images = files(startsWith(files, 'r')); % RGB files
accel_data = files(startsWith(files, 'a')); % Accel files
% Getting number of files
num_depth = length(depth_images); % Number of depth files
num_rgb = length(rgb_images); % Number of RGB files
num_accel = length(accel_data); % Number of accel data files

fprintf('Found %d depth, %d rgb images, and %d accel dumps.\n', ...
    num_depth, num_rgb, num_accel);

% Getting tuples of RGB and Depth images and Accelerometer Data based on
% timestamps
jj = 1; % Current RGB pointer.
kk = 1; % Current Accel pointer.
for ii = 1 : num_depth
    fprintf('Matching depth image %d/%d\n', ii, num_depth);
    
    % Parsing the timestamps
    time_parts_depth = strsplit(extractAfter(depth_images(ii), 2), '-');
    time_parts_rgb = strsplit(extractAfter(rgb_images(ii), 2), '-');
    time_parts_accel = strsplit(extractAfter(accel_data(ii), 2), '-');
    
    t_depth = str2double(time_parts_depth{1});
    t_rgb = str2double(time_parts_rgb{1});
    t_accel = str2double(time_parts_accel{1});
    
    t_diff = abs(t_depth-t_rgb);
    % Advancing the curInd until the difference in times gets worse
    while jj < num_rgb
        time_parts_rgb = strsplit(extractAfter(rgb_images(jj+1), 2), '-');
        t_rgb = str2double(time_parts_rgb{1});
        
        tmp_diff = abs(t_depth-t_rgb);
        if tmp_diff > t_diff
            break;
        end
        t_diff = tmp_diff;
        
        % Otherwise, its better! and we should update jj
        jj = jj + 1;
    end
    
    t_diff = abs(t_depth-t_accel);
    % Advancing the curInd until the difference in times gets worse
    while kk < num_accel
        time_parts_accel = strsplit(extractAfter(accel_data(kk+1), 2), '-');
        t_accel = str2double(time_parts_accel{1});
        
        tmp_diff = abs(t_depth-t_accel);
        if tmp_diff > t_diff
            break;
        end
        t_diff = tmp_diff;
        
        % Otherwise, its better! and we should update kk
        kk = kk + 1;
    end
    
    fprintf('Matched depth %d to rgb %d and accel %d.\n', ii, jj, kk);
    
    % Returning Depth Images, RGB Images and Accelerometer Data of the
    % current Frame
    frame_list(ii).depth_image = depth_images(ii);
    frame_list(ii).rgb_image = rgb_images(jj);
    frame_list(ii).accel_rec = accel_data(kk);
end
fprintf('\n');
end
