% Demo's the in-painting function fill_depth_cross_bf.m


%%
for i = 1:1449
imageInd = i;

imgRgb = images(:,:,:,imageInd);
imgDepthAbs = rawDepths(:,:,imageInd);

% Crop the images to include the areas where we have depth information.
imgRgb = crop_image(imgRgb);
imgDepthAbs = crop_image(imgDepthAbs);

imgDepthFilled = fill_depth_cross_bf(imgRgb, double(imgDepthAbs));

imagesc(imgRgb);
set(gca,'XTick',[]) % Remove the ticks in the x axis!
set(gca,'YTick',[]) % Remove the ticks in the y axis
set(gca,'Position',[0 0 1 1]) % Make the axes occupy the hole figure
print(gcf, '-djpeg99', ['./imgs/', num2str(i), '_rgb'])
%imagesc(imgDepthAbs);
imagesc(imgDepthFilled);
set(gca,'XTick',[]) % Remove the ticks in the x axis!
set(gca,'YTick',[]) % Remove the ticks in the y axis
set(gca,'Position',[0 0 1 1]) % Make the axes occupy the hole figure
print(gcf, '-djpeg99', ['./imgs/', num2str(i), '_map'])
disp(i)
end