% This Script processes NYUv2 RAW Dataset and creates RGB + Depth Map Image Tuples
%
% For this script to work please follow the following instruction:
% 1) First of all download RAW Dataset ('Raw dataset, Single File
%    (~428 GB)') from https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html
% 2) Extract the zip file into a folder
% 3) Download Matlab scripts ('tools') from https://bitbucket.org/madmenai/projecticarus
%    (Do not use the ones available on the NYU Website, as they
%    might not work on some platforms)
% 4) Create a folder called 'tools' in the folder where dataset was extracted
% 5) Put downloaded Matlab scripts into the 'tools' folder
% 6) Put this file into the folder where dataset was extracted
% 7) You now should have the following folder structure:
%       root_folder
%           |- subset_1_01
%           |- subset_1_02
%           |- ...
%           |- subset_1_N
%           |- ...
%           |- subset_M_N
%           |- tools
%           |- process_dataset.m
% 8) Compile mex files on your platform
% 9) You can now run this file ('process_dataset.m') with Matlab
%
% Things you can change in this script:
% image_step: image count to skip during preprocessing (image_step = 1 ->
% preprocess all images)

% Clearing our memory and terminal
clc; clear all;
addpath('tools'); % Adding 'tools' Folder to our Path

fprintf('Starting Data Pre-Procession\n');
image_step = 1; % How many frames to skip during preprocessing
current_dir = dir('.'); % Getting Content of the Current Folder
fprintf('Searching for Folders\n');
subfolders = [current_dir(:).isdir]; % Getting Subdirectories
subfolders = {current_dir(subfolders).name}'; % Getting Names of the Subdirectories
subfolders(ismember(subfolders,{'.','..','tools'})) = []; % Excluding '.', '..', and 'tools' Pathes
fprintf("[%d] Folders have been found!\n", numel(subfolders));

processed_images = 0;

for f = 1:numel(subfolders)
    fprintf("\n---------------------------------------------------\n");
    fprintf("Processing Subfolder: %s\n", subfolders{f});
    files = get_synched_frames(subfolders{f}); % Getting a struct of synchronized frames and accel data
    file_count = numel(files); % Number of frames
    fprintf('Total # of Files: %d\n',file_count);
    files = files(1:image_step:file_count); % Dropping out some frames
    file_count = numel(files); % Number of frames after the dropout
    fprintf('# of Files to Process: %d\n', file_count);
    out_folder = strcat(subfolders{f}, '_out'); % Folder where preprocessed files will be stored
    
    % Checking if the output folder exists, if not, creating it
    if ~exist(out_folder, 'dir')
        mkdir(out_folder);
    end
    
    % Starting a parallel for loop for data pre-processing
    parfor idx = 1:file_count
        % Getting files
        filename_rgb = strcat(subfolders{f},'/',files(idx).rgb_image);
        filename_depth = strcat(subfolders{f},'/',files(idx).depth_image);
        % Creating filenames for new data files
        out_filename_rgb = strcat(subfolders{f},'_out/',subfolders{f},num2str(idx),'rgb.png');
        out_filename_depth = strcat(subfolders{f},'_out/',subfolders{f},num2str(idx),'depth.png');
        % Reading RGB and Depth files
        rgb = imread(char(filename_rgb));
        depth = imread(char(filename_depth));
        % Converting little-endian values to big-endian
        depth = swapbytes(depth);
        % Projecting depth values on an RGB Image
        [depth_out, img_rgb] = project_depth_map(depth, rgb);
        % Preprocessing and smoothing the depth image using a gray scale version of the RGB image
        img_depth = fill_depth_colorization(double(img_rgb) / 255.0, depth_out, 0.8);
        img_depth = img_depth / 10.0;
        % Cropping the images to use only those parts, which have depth data
        img_depth = crop_image(img_depth);
        img_rgb = crop_image(img_rgb);
        % Saving the images
        imwrite(img_rgb, out_filename_rgb);
        imwrite(img_depth, out_filename_depth);
    end
    
    fprintf("Images Processed: %d\n", file_count);
end
