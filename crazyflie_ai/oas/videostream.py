import socket
import cv2
import numpy as np
from threading import Thread, Lock


class VideoStream(Thread):
	"""  """

	def __init__(self, host, port, jpeg_quality=90):
		super(VideoStream, self).__init__()
		self.server_address = (host, port)

		self.lock = Lock()
		self.buffer = None
		self.encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), jpeg_quality]

	def run(self):
		with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
			sock.bind(self.server_address)

			client_conn, client_address = sock.recvfrom(1)
			print('Receiving from:', client_address)

			while True:
				self.lock.acquire()
				if self.buffer is not None:
					sock.sendto(self.buffer.tobytes(), client_address)
					if self.buffer == b'quit':
						break
				self.lock.release()

	def send(self, img1, img2):
		self.lock.acquire()
		montage = self._montage_images(img1, img2)
		_, self.buffer = cv2.imencode('.jpg', montage, self.encode_param)
		self.lock.release()

	def stop(self):
		self.buffer = b'quit'

	def _montage_images(self, img1, img2, shape=(320, 240)):
		img1 = np.array(img1)
		img2 = np.array(img2)
		img1 = cv2.cvtColor(img1.astype(np.uint8), cv2.COLOR_RGB2BGR)
		img2 = cv2.cvtColor(img2.astype(np.uint8), cv2.COLOR_GRAY2BGR)
		img1 = cv2.resize(img1, shape)
		img2 = cv2.resize(img2, shape)
		img2 = cv2.applyColorMap(img2, cv2.COLORMAP_PARULA)
		montage = np.concatenate((img1, img2), axis=1)
		return montage
