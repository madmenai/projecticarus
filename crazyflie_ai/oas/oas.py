""" This notebook is a real-time depth prediction and flight control system """

# Imports
import os  # Misc OS Functions
import system  # System Functions
import cv2  # OpenCV - Computer Vision Library
import time  # Time Library
import models  # ANN Models
import keyboard  # Keyboard Library
import numpy as np  # Scientific Library
import collections  # Additional Containers
import tensorflow as tf  # Numerical Library for AI Modelling
from PIL import Image  # Image Processing Library
from datetime import datetime  # Datetime Library
from easydict import EasyDict as edict  # Dictionary Lib
from scripts.post_processing import process_image  # Image Post-Processing Functions
# Crazyflie Libs
import cflib.crtp  # Crazyflie Communication Library
from videostream import VideoStream  # Video Stream Library
from cflib.crazyflie import Crazyflie  # Crazyflie Class
from cflib.crazyflie.log import LogConfig  # Crazyflie Logging Library

# Constants
CAMERA_ID = 0  # Device ID of the Main Camera
KEY_ESC = 'esc'
UDP_DEFAULT_IP = '192.168.100.6'
UDP_DEFAULT_PORT = 16000
DEFAULT_JPEG_QUALITY = 70

# Global Variables
coordinates = edict({'x': 0, 'y': 1})  # Dictionary of Coordinate Indices in a Coordinate Tuple
cf_current_height = 0  # Real Height of the Drone
cf_current_voltage = 0  # Voltage of the Drone battery
cf_ready = True  # Status of the drone
tf_session = None  # Current Environment for Numerical Operations
tf_input_layer = None  # Input Layer of our Neural Network
tf_network = None  # Neural Network used for making Predictions
# Height of an Image which will be passed to the Input Layer of our Neural Network
tf_input_height = 228
# Width of an Image which will be passed to the Input Layer of our Neural Network
tf_input_width = 304
# Channel Count of an Image which will be passed to
# the Input Layer of our Neural Network
tf_input_channels = 3
tf_input_batch_size = 1  # We are making a Prediction for one Image at a time
#  Path to a Folder where a Model is located
path_models = r'/home/george/projects/master/oas/models/tf_models/NYU_FCRN-checkpoint/NYU_FCRN.ckpt'
# Path to a Folder where Captured Frame Bundles will be Saved
path_experiments = r'/home/george/projects/master/oas/frames/'
# Path to a Folder where Captured Frames will be saved
path_frames = ''
drone = None  # Drone Object
video_stream = None  # Video Stream Object


class Drone:
    """ Drone Class """
    # Constants
    DEFAULT_URI = 'radio://0/80/2M'  # URI of the Crazyflie Drone
    YAW = 8  # Yaw Value
    PITCH = 0.25  # Pitch Value
    HEIGHT = 0.50  # Flight Height
    TEST_HEIGHT = 0.20  # Max Possible Height Before the Take-Off
    CRITICAL_VOLTAGE = 2.4  # Critical Voltage below which the Drone will not work
    MIN_LAND_HEIGHT = 0.05  # 5cm, safe height, when we can turn off the motors
    LAND_STEP = 0.05  # 5cm, landing step
    # Crazyflie Variables to Log
    LOG_VARIABLES = [edict({'name': 'stateEstimate.z', 'type': 'float'}),
                     edict({'name': 'pm.vbat', 'type': 'float'})]

    def __init__(self, link_uri, log_vars):
        self.link_uri = link_uri
        self.sensors = edict({'height': -1, 'voltage': -1})
        self.cf = None
        self.log_parameters = None
        self.log_vars = log_vars
        self.ready = True
        self.is_connected = False
        self.voltage_values = collections.deque(maxlen=10)
        self.initialize()

    def initialize(self):
        """ Initializes a Crazyflie Object and its Callbacks """
        # Creating a Crazyflie Object and Initializing Crazyflie Drivers
        print('Initializing drivers...')
        cflib.crtp.init_drivers(enable_debug_driver=False)
        self.cf = Crazyflie(rw_cache='./cache')

        # Connecting callbacks from the Crazyflie API
        print('Creating callbacks...')
        self.cf.connected.add_callback(self.connected)
        self.cf.disconnected.add_callback(self.disconnected)
        self.cf.connection_failed.add_callback(self.connection_failed)
        self.cf.connection_lost.add_callback(self.connection_lost)
        self.connect()
        print('Waiting for the drone to initialize...')

        while self.sensors.voltage == -1:
            pass

    # Crazyflie Callbacks
    def connected(self, link_uri):
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded """
        print('Connected to %s' % link_uri)
        self.is_connected = True

        # The definition of the logconfig can be made before connecting
        self.log_parameters = LogConfig(name='SensorsData', period_in_ms=10)

        for log_var in self.log_vars:
            self.log_parameters.add_variable(log_var.name, log_var.type)

        # Adding Z Coord Logging
        try:
            self.cf.log.add_config(self.log_parameters)
            # This callback will receive the data
            self.log_parameters.data_received_cb.add_callback(self.log_on_data_receive)
            # This callback will be called on errors
            self.log_parameters.error_cb.add_callback(self.log_on_error)
            # Start the logging
            self.log_parameters.start()
        except KeyError as e:
            print('Could not start log configuration,'
                  '{} not found in TOC'.format(str(e)))
        except AttributeError:
            print('Could not add Stabilizer log config, bad configuration.')

        time.sleep(1)

    def voltage_add_value(self, value):
        """ Adds a new value to the list of voltage values and calculates an average voltage """
        self.voltage_values.append(value)
        self.sensors.voltage = np.mean(self.voltage_values)

    @staticmethod
    def log_on_error(logconf, msg):
        """ Callback from the log API when an error occurs """
        print('Error when logging %s: %s' % (logconf.name, msg))

    def log_on_data_receive(self, _, data, *__):
        """ Callback from the log API when data arrives """
        self.sensors.height = data['stateEstimate.z']
        self.voltage_add_value(data['pm.vbat'])

    def connection_failed(self, link_uri, msg):
        """ Callback when connection initial connection fails (i.e no Crazyflie
        at the speficied address)"""
        print('Connection to %s failed: %s' % (link_uri, msg))
        self.is_connected = False

    def connection_lost(self, link_uri, msg):
        """ Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range) """
        print('Connection to %s lost: %s' % (link_uri, msg))
        self.is_connected = False

    def disconnected(self, link_uri):
        """ Callback when the Crazyflie is disconnected (called in all cases) """
        print('Disconnected from %s' % link_uri)
        self.is_connected = False

    def emergency_land(self):
        """Emergency landing routine"""
        cf_local_z = self.sensors.height

        while cf_local_z > self.MIN_LAND_HEIGHT:
            cf_local_z -= self.LAND_STEP
            self.cf.commander.send_hover_setpoint(0, 0, 0, cf_local_z)
            time.sleep(0.1)

        self.shutdown()
        self.ready = False
        self.is_connected = False

    def connect(self):
        """ Connects to the Crazyflie Drone """
        print('Connecting to the drone...')
        self.is_connected = False
        self.cf.open_link(self.link_uri)

        while not self.is_connected:
            pass

    def is_ready(self):
        """ Indicates if Drone is ready for a flight """
        return self.ready

    def self_test(self):
        """ Tests Crazyflie Sensors """
        print('Starting sensors test...')
        status_text = ''
        time.sleep(3)

        if cf_current_height >= self.TEST_HEIGHT:
            self.ready = False
            status_text += 'LASER TEST FAILED!\n'
            print(status_text)

        if cf_current_voltage <= self.CRITICAL_VOLTAGE:
            self.ready = False
            status_text += 'VOLTAGE TEST FAILED!\n'
            print(status_text)

        if not self.ready:
            print('Terminating the script!')
            system.sys.exit()

        print('Test passed!')

    def take_off(self):
        """ Drone take-off  routine """
        print('Starting a take-off routine...')
        for i in range(0, 20):
            time.sleep(0.2)
            self.set_state(0, 0, 0, self.HEIGHT)

    def is_critical_state(self):
        """ Checks if the drone is in a critical state """
        if self.sensors.voltage < self.CRITICAL_VOLTAGE:
            print('The drone is in a critical state! Starting an emergency landing.')
            self.emergency_land()

    def set_state(self, pitch, roll, yaw, height):
        """ Sets new state """
        self.cf.commander.send_hover_setpoint(pitch, roll, yaw, height)

    def shutdown(self):
        """ Shutdowns the drone """
        print('Shutting down the drone...')
        self.cf.commander.send_stop_setpoint()


class Camera:
    """ Camera Class """
    DEFAULT_ID = 0  # Default Camera ID
    DEFAULT_FPS = 24  # Camera FPS

    def __init__(self, cam_id, fps):
        self.id = cam_id
        self.video = cv2.VideoCapture(self.id)
        self.video.set(cv2.CAP_PROP_FPS, fps)

    def get_frame(self):
        _, frame = self.video.read()
        return frame


def prepare_folders():
    """ Creates necessary folders """
    global path_experiments, path_frames
    print('Preparing necessary folders...')
    generate_paths()

    if not os.path.isdir(path_experiments):
        os.mkdir(path_experiments)

    print('Done!')


def generate_paths():
    """ Generates folder paths """
    global path_frames
    path_frames = os.path.join(path_experiments, 'exp_{}'.format(datetime.now().strftime('%d_%m_%Y_%H_%M_%S')))
    os.mkdir(path_frames)


def render_video():
    """ Converts images into a video file """
    # Converting images into video
    print('Converting images into a video...')
    video_codec = cv2.VideoWriter_fourcc(*'XVID')
    video_writer = cv2.VideoWriter(os.path.join(path_frames, 'out.avi'), video_codec, 15, (1280, 480))
    frame_list = sorted(os.listdir(path_frames))
    map_list = [f for f in frame_list if 'map' in f]
    frame_list = [f for f in frame_list if 'frame' in f]

    for (frame, depth_map) in zip(frame_list, map_list):
        frame_real = cv2.imread(os.path.join(path_frames, frame), cv2.IMREAD_COLOR)
        frame_prediction = cv2.applyColorMap(
            cv2.resize(cv2.imread(os.path.join(path_frames, depth_map), cv2.IMREAD_GRAYSCALE), (640, 480)),
            cv2.COLORMAP_PARULA)
        frame_combined = np.concatenate((frame_real, frame_prediction), axis=1)
        video_writer.write(frame_combined)

    video_writer.release()
    cv2.destroyAllWindows()
    print('Done!')


def prepare_nn():
    """ Loads and Prepares a Neural Network """
    global tf_session
    global tf_input_layer
    global tf_network
    # Importing a CNN Model
    # Creating a TF Session
    tf_session = tf.Session()
    print('Loading the model...')
    # Creating an Input Layer for our Neural Network
    tf_input_layer = tf.placeholder(
        tf.float32, shape=(None, tf_input_height, tf_input_width, tf_input_channels))
    # Building our Neural Network
    tf_network = models.ResNet50UpProj(
        {'data': tf_input_layer}, tf_input_batch_size, 1, False)
    # Loading Parameters of the Network from Files
    saver = tf.train.Saver()
    saver.restore(tf_session, path_models)
    print('Done!')


def drone_init(uri=Drone.DEFAULT_URI, log_vars=Drone.LOG_VARIABLES):
    """ Initializes a Drone Object """
    global drone
    print('Initializing the drone...')
    drone = Drone(uri, log_vars)
    print('Done!')


def camera_init(cam_id=Camera.DEFAULT_ID, fps=Camera.DEFAULT_FPS):
    """ Initializes a Camera """
    global camera
    print('Initializing the camera...')
    camera = Camera(cam_id, fps)
    print('Done')


def keyboard_init():
    """ Initializes keyboard callbacks """
    print('Initializing keyboard callbacks...')
    keyboard.on_press(on_key_clicked)
    print('Done!')


def video_stream_init(ip=UDP_DEFAULT_IP, port=UDP_DEFAULT_PORT, quality=DEFAULT_JPEG_QUALITY):
    """ Initializes a video streaming thread """
    global video_stream
    print('Initializing video streaming...')
    video_stream = VideoStream(ip, port, quality)
    video_stream.start()
    print('Done!')


def initialize():
    """ Initializes modules """
    prepare_folders()
    prepare_nn()
    camera_init()
    keyboard_init()
    video_stream_init()
    drone_init()


def on_key_clicked(event):
    """ Key click callback """
    global drone
    if event.name is KEY_ESC:
        drone.ready = False


def start_oas():
    """ Starts the OAS """
    global camera, drone
    assert camera is not None, 'Please initialize a Camera!'
    assert drone is not None, 'Please initialize a Drone!'

    drone.take_off()
    print('Starting the OAS...')
    # predictions = []  # List of Predictions
    frame_number = 0  # Current Frame number

    # Starting depth prediction
    while drone.is_ready() and not drone.is_critical_state():
        drone.set_state(0, 0, 0, Drone.HEIGHT)
        frame_number += 1
        # Getting a Frame from the Camera
        cv_original_frame = camera.get_frame()  # Original Image captured from the Camera
        cv2.imwrite(os.path.join(path_frames, 'frame-%.5d.png' % frame_number), cv_original_frame)

        if cv_original_frame is not None:
            # Converting an Array to an Image
            in_image = Image.fromarray(cv_original_frame)
            # Resizing the Image so it matches the Input Layer Dimensions
            image = in_image.resize([tf_input_width, tf_input_height],
                                    Image.ANTIALIAS)
            # Converting Pixel Values to a `float32` Type
            image = np.array(image).astype('float32')
            # Adding a new Axis
            image = np.expand_dims(np.asarray(image), axis=0)
            # Making our Prediction for the Current Image
            prediction = tf_session.run(tf_network.get_output(),
                                        feed_dict={tf_input_layer: image})
            # Saving our Prediction
            image = prediction[0, :, :, 0]
            image = image * 255 / image.max()
            cv2.imwrite(os.path.join(path_frames, 'map-%.5d.png' % frame_number), image)
            depth_map, image_center, map_center = process_image(image)

            diff = (map_center[coordinates.x] - image_center[coordinates.x]) / 30
            #         print(diff)
            #         predictions.append(diff)

            #         if len(predictions) == 10:
            #             direction = 1 if sum(predictions) > 0 else - 1
            #             predictions = []

            # Setting new Flight Parameters
            drone.set_state(Drone.PITCH, 0, Drone.YAW * diff, Drone.HEIGHT)
            video_stream.send(np.array(in_image), image)

        drone.shutdown()


if __name__ == '__main__':
    """ Main Entry Point """
    initialize()
    start_oas()
