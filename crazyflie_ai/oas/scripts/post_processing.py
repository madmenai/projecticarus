""" This file contains post-processing functions
    for predictions made by a ANN model """

# Imports
import cv2
import numpy as np
from skimage import measure

# Constants
MAX_THRESHOLD = 255
MIN_THRESHOLD = 60
THRESHOLD_STEP = 25
LINSPACE_THRESHOLDS = np.linspace(MAX_THRESHOLD, MIN_THRESHOLD, THRESHOLD_STEP, dtype='int')
LOGSPACE_THRESHOLDS = np.logspace(np.log10(MAX_THRESHOLD), np.log10(MIN_THRESHOLD), dtype='int')


# Functions

def try_read_image(image):
    """ Checks if the 'image' argument is an image path,
        and if true, reads this image """
    if type(image) is str:
        image = cv2.imread(image)
    elif type(image) is not np.ndarray:
        return None

    return image


def convert_to_grayscale(image):
    """ Converts an RGB image to a grayscale """
    # Checking if an input image is not grayscale
    assert type(image) is np.ndarray, 'Image should be in a numpy.ndarray format!'

    if image.shape[-1] == 3:
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    return image


def find_deep_spots(image, check_center, log_space=True, use_dilation=False, use_opening=False):
    """ Finds deep areas on the depth map """
    # Parameters
    min_area = 0.05  # 5% of the image
    max_area = 0.80  # 80% of the image

    # Parameters are different when checking depth only in the center of the image
    if check_center:
        min_area = 0.30  # 30% of the image
        max_area = 0.60  # 60% of the image

    # Trying to read the image
    image = try_read_image(image)

    # Checking if the image was successfully read
    if image is None:
        return None

    # Converting the RGB image to grayscale
    image = convert_to_grayscale(image)

    # Check only the center of the image?
    if check_center:
        height = image.shape[0]
        offset = height // 4
        bottom = offset
        top = height - offset
        image = image[bottom:top, 0:image.shape[1]]

    # Trying to find deep spots
    if log_space:
        thresholds = LOGSPACE_THRESHOLDS
    else:
        thresholds = LINSPACE_THRESHOLDS

    total_pixels = image.shape[0] * image.shape[1]
    best_map = None

    if use_dilation:
        kernel = np.ones((10, 10), np.uint8)
        image = cv2.dilate(image, kernel, iterations=1)

    if use_opening:
        kernel = np.ones((10, 10), np.uint8)
        image = cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel)

    # Processing the image with different thresholds
    for threshold in thresholds:
        img_thresh = cv2.threshold(image, threshold, 255, cv2.THRESH_BINARY)[1]
        img_thresh = cv2.erode(img_thresh, None, iterations=2)
        img_thresh = cv2.dilate(img_thresh, None, iterations=4)
        img_thresh = convert_to_grayscale(img_thresh)
        white_pixels = np.sum(img_thresh == 255)
        white_area = white_pixels / total_pixels

        if min_area <= white_area <= max_area:
            best_map = img_thresh
            break

    return best_map


def get_center(img):
    """ Calculates a center point of an image """
    x_center = img.shape[1] / 2
    y_center = img.shape[0] / 2

    return x_center, y_center
    

def get_map_center(img):
    """ Calculates a center point of a white area on an image """
    y, x = np.nonzero(img)
    x_mean = x.mean()
    y_mean = y.mean()

    return x_mean, y_mean


def process_image(img):
    """ Runs a post-processing routine on an image """
    # Calculating a depth map
    depth_map = find_deep_spots(img,
                                check_center=False,
                                log_space=False,
                                use_opening=True,
                                use_dilation=True)

    if depth_map is None:
        return None, None

    # Getting the biggest white spot on the depth map
    areas = np.array(measure.label(depth_map, neighbors=8, background=0))
    hist = np.bincount(areas.flatten())
    idx = max(range(1, len(hist)), key=lambda x: hist[x])
    depth_map = areas == idx

    # Calculating trajectory
    image_center = get_center(depth_map)
    map_center = get_map_center(depth_map)

    return depth_map, image_center, map_center
