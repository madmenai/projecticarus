# Imports
import os
import numpy as np
import cv2
import tensorflow as tf
from keras.layers import Conv2D, Conv2DTranspose, Activation, BatchNormalization, MaxPooling2D, Dense, Reshape, Flatten, Dropout, UpSampling2D
from keras.models import Sequential
from keras.backend.tensorflow_backend import set_session
from math import ceil
from pathlib import Path
from datetime import datetime

DIR_DATA = '/media/slowdrive/nyu_dataset/nyu_full_processed/'  # Dataset Directory
DIR_MODELS = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'models')
FILE_NPY_XVAL = Path(os.path.join(DIR_DATA, 'x_val.npy'))  # X_Val NPY File
FILE_NPY_YVAL = Path(os.path.join(DIR_DATA, 'y_val.npy'))  # Y_Val NPY File
LOAD_NPY = True  # Flag that indicates if NPY files should be loaded if they exist
RGB_HEIGHT = 228  # Height of an RGB image
RGB_WIDTH = 304  # Width of an RGB image
DEPTH_HEIGHT = 50  # Height of a depth map image
DEPTH_WIDTH = 52  # Width of a depth map image

config = tf.ConfigProto()
config.gpu_options.allow_growth = True 
config.gpu_options.allocator_type = 'BFC'
sess = tf.Session(config=config)
set_session(sess)

# Uncomment the lines below to run this script on a CPU
# os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
# os.environ['CUDA_VISIBLE_DEVICES'] = ''

# Global variables
pbar = None  # Progress Bar
is_console = False  # Flag that indicates if the script is running in a console

# Checking if the current script is running in a console or in a Jupyter Notebook
try:
    get_ipython
    from progress_bar import ProgressBar
except:
    is_console = True


# Some handy functions
def get_files_from_dir(directory):
    """ Returns a list of absolute pathes to files in directory """
    paths = []
    
    for dirpath, _, filenames in os.walk(directory):
        for f in filenames:
            paths.append(os.path.abspath(os.path.join(dirpath, f)))
    
    return paths


def check_frame_synchronization(list_rgb, list_depth):
    """ Checks if frames are synchronized """
    synch_ok = True

    for i in range(0, len(list_rgb)):
        rgb_name = list_rgb[i].replace('rgb.png', '')
        depth_name = list_depth[i].replace('depth.png', '')

        if rgb_name != depth_name:
            print(rgb_name)
            print(depth_name)
            synch_ok = False
            break

    if synch_ok:
        print('Frame synchronization check: PASSED')
    else:
        print('Frame synchronization check: FAILED')
        

def print_progress_bar(iteration, total_iterations, prefix='', suffix='', decimals=1, length=100, fill='█'):
    """ Displays a progress bar in console window """
    percent = ('{0:.' + str(decimals) + 'f}').format(100 * (iteration / float(total_iterations)))
    filled_length = int(length * iteration // total_iterations)
    bar = fill * filled_length + '-' * (length - filled_length)
    print('\r{} |{}| {}% {}'.format(prefix, bar, percent, suffix), end='\r')
    

def data_generator(rgb_data, depth_data, batch_size):
    """ A generator to load batches of images from the dataset """
    list_size = len(rgb_data)

    # Keras needs a generator to be infinite
    while True:
        batch_start = 0
        batch_end = batch_size
        
        while batch_start < list_size:
            limit = min(batch_end, list_size)
            x_train = []
            y_train = []

            for i in range(batch_start, limit):
                image = cv2.imread(x_train_names[i])
                image = cv2.resize(image, (RGB_WIDTH, RGB_HEIGHT))
                x_train.append(image)
                image = cv2.imread(y_train_names[i], cv2.IMREAD_GRAYSCALE)
                image = cv2.resize(image, (DEPTH_WIDTH, DEPTH_HEIGHT))
                y_train.append(image)
            
            yield (np.array(x_train), np.array(y_train))

            batch_start += batch_size   
            batch_end += batch_size


def huber_loss(y_true, y_pred, clip_delta=1.0):
    error = y_true - y_pred
    cond  = tf.keras.backend.abs(error) < clip_delta
    squared_loss = 0.5 * tf.keras.backend.square(error)
    linear_loss  = clip_delta * (tf.keras.backend.abs(error) - 0.5 * clip_delta)

    return tf.where(cond, squared_loss, linear_loss)


# Getting Pathes to the image files
print('Getting pathes to the image files...')
files = get_files_from_dir(DIR_DATA)
print('A total of {} files were found!'.format(len(files)))

# Creating lists of RGB and Depth images
print('Creating lists of RGB and Depth images...')
rgb_images = [file for file in files if 'rgb' in file]
depth_images = [file for file in files if 'depth' in file]

# Sorting the images
print('Sorting the images...')
rgb_images = sorted(rgb_images)
depth_images = sorted(depth_images)

# Checking if sizes of rgb and depth image lists are identical
print('Checking if sizes of rgb and depth image lists are identical...')

if len(rgb_images) == len(depth_images):
    print('List size check: PASSED')
else:
    print('List size check: FAILED')

# Checking if corresponding frames are located at the same indicies
print('Checking if frames are synchronized...')
check_frame_synchronization(rgb_images, depth_images)

# Converting python lists to numpy lists
rgb_images = np.array(rgb_images)
depth_images = np.array(depth_images)

# Shuffling the arrays
print('Shuffling the arrays...')
shuffled_indices = np.arange(0, len(rgb_images))
np.random.shuffle(shuffled_indices)
rgb_images = rgb_images[shuffled_indices]
depth_images = depth_images[shuffled_indices]

# Creating train sets
print('Creating train sets...')
split = 0.70
split_idx = int(len(rgb_images) * split)
x_train_names = rgb_images[:split_idx]
y_train_names = depth_images[:split_idx]
x_val_names = rgb_images[split_idx:]
y_val_names = depth_images[split_idx:]

print('Model will be trained on {} images and validated on {} images'.format(len(x_train_names), len(x_val_names)))

# Rechecking frame synchronization
print('Re-checking if frames are synchronized...')
check_frame_synchronization(x_train_names, y_train_names)
check_frame_synchronization(x_val_names, y_val_names)

if LOAD_NPY and FILE_NPY_XVAL.is_file() and FILE_NPY_YVAL.is_file():
    # Loading validation sets
    print('Loading validation sets...')
    x_val = np.load(FILE_NPY_XVAL)
    y_val = np.load(FILE_NPY_YVAL)
else:
    # Creating validation sets
    x_val = []
    y_val = []
    total_images = len(x_val_names) + len(y_val_names)
    current_image_idx = 0
    
    if not is_console:
        pbar = ProgressBar(total_elements=len(x_val_names) + len(y_val_names),
                           increment_value=1,
                           step=1,
                           info_text='Progress')
        pbar.show()
        
    print('Creating validation sets...')
    
    for img in x_val_names:
        image = cv2.imread(img)
        image = cv2.resize(image, (RGB_WIDTH, RGB_HEIGHT))
        x_val.append(image)
        
        if is_console:
            current_image_idx += 1
            print_progress_bar(current_image_idx, total_images - 1, '> Progress:', '', length=30)
        else:
            pbar.increment_update()

    for img in y_val_names:
        image = cv2.imread(img, cv2.IMREAD_GRAYSCALE)
        image = cv2.resize(image, (DEPTH_WIDTH, DEPTH_HEIGHT))
        y_val.append(image)
        
        if is_console:
            current_image_idx += 1
            print_progress_bar(current_image_idx, total_images - 1, '> Progress:', '', length=30)
        else:
            pbar.increment_update()

    x_val = np.array(x_val)
    y_val = np.array(y_val)

    print('\nSaving arrays to files...')
    np.save(str(FILE_NPY_XVAL), x_val)
    np.save(str(FILE_NPY_YVAL), y_val)
    
print('Done!')

# Creating CNN Models
print('Creating a CNN model...')
models = {}  # Dictionary of models

# Model 1 - Name: CNN_01
model = None
model = Sequential()
model.add(Conv2D(kernel_size=11, filters=96, strides=4, input_shape=(228, 304, 3)))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2, 2), strides=2))
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=5, filters=256))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2, 2), strides=2))
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=384))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=384, strides=2))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=256, strides=2))
model.add(Flatten())
model.add(Dense(4096, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(4144, activation='sigmoid'))
model.add(Dropout(0.2))
model.add(Reshape((56, 74)))
models['CNN_01'] = model

# Model 2 - Name: CNN_02
model = None
model = Sequential()
model.add(Conv2D(kernel_size=5, filters=32, strides=1, activation='relu', input_shape=(228, 304, 3)))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2, 2), strides=2))
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=64, activation='relu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size=(2, 2), strides=2))
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=128, activation='relu'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=2, filters=128, strides=2, activation='relu'))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=512, strides=2, activation='relu'))
model.add(Flatten())
model.add(Dense(1000, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(4144, activation='sigmoid'))
model.add(Dropout(0.2))
model.add(Reshape((56, 74)))
models['CNN_02'] = model

# Model 3 - Name: CNN_03
model = None
model = Sequential()
model.add(Conv2D(kernel_size=3, filters=32, strides=2, use_bias=False, input_shape=(228, 304, 3)))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=64, strides=2, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=128, strides=2, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=128, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=256, strides=2, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=256, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=512, strides=2, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(UpSampling2D(size=2))
model.add(Conv2DTranspose(kernel_size=3, filters=256, strides=2))
model.add(Conv2D(kernel_size=3, filters=512, strides=2, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2DTranspose(kernel_size=3, filters=256, strides=2))
model.add(Conv2D(kernel_size=3, filters=256, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2DTranspose(kernel_size=3, filters=128, strides=2))
model.add(Conv2D(kernel_size=3, filters=256, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2DTranspose(kernel_size=3, filters=64, strides=2))
model.add(Conv2D(kernel_size=3, filters=256, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2DTranspose(kernel_size=3, filters=32, strides=2))
model.add(Conv2DTranspose(filters=1, kernel_size=3, strides=1))
model.add(Conv2DTranspose(filters=1, kernel_size=3, strides=1))
model.add(Conv2D(kernel_size=3, filters=256, use_bias=False))
model.add(BatchNormalization())
model.add(Activation('relu'))
model.add(Conv2D(kernel_size=3, filters=32, strides=2))
model.add(Conv2D(kernel_size=3, filters=16))
model.add(Conv2DTranspose(filters=8, kernel_size=3, strides=3))
model.add(Conv2DTranspose(filters=1, kernel_size=3, strides=1))
model.add(Conv2D(kernel_size=3, filters=16, strides=3))
model.add(Conv2D(kernel_size=3, filters=8, strides=2))
model.add(Conv2D(kernel_size=3, filters=2, strides=2))
model.add(UpSampling2D(size=2))
model.add(Reshape((DEPTH_HEIGHT, DEPTH_WIDTH)))
models['CNN_03'] = model
    
# We can now compile any of our model and run a training process
# Selecting a model first
curr_model = 'CNN_03'
model = models[curr_model]

# Creating some parameters for a 'fit_generator' function
batch_size = 32
epochs = 100
steps_per_epoch = ceil(len(x_train_names) / batch_size)

# Printing a model summary
print('Model Summary')
model.summary()
print('Batch Size: {}\nEpochs: {}\nSteps Per Epoch: {}\n'.format(
    batch_size, epochs, steps_per_epoch
))

# Compiling it
print('Compilling the model...')
for params in [[huber_loss, 'adam'], ['msle', 'sgd'], ['msle', 'rmsprop']]:
    model.compile(loss=params[0], optimizer=params[1], metrics=['accuracy'])

    # Running model training
    print('Starting a training process...')
    model.fit_generator(data_generator(x_train_names, y_train_names, batch_size),
                        validation_data=(np.array(x_val), np.array(y_val)),
                        epochs=epochs,
                        steps_per_epoch=steps_per_epoch)

    print('Saving the model...')
    model_file = os.path.join(DIR_MODELS, 'model_{}_{}.h5'.format(params[1], str(datetime.now())))
    model.save(model_file)
    print('Model has been saved to {}.'.format(model_file))

print('The End!')
